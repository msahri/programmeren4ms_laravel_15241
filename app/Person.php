<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    public function country(){
        return $this->belongsTo('App\Country');
    }
    
    public function user(){
        return $this->hasMany('App\User');
    }
}
