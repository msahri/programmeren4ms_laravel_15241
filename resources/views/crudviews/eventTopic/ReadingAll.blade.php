@extends('layouts.crud')

@section('title')
        <title>Onderwerp</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/eventTopic">Event Onderwerp</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventTopicsController@create") }}">Onderwerp Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    @if(count($eventtopic) > 0)
            @foreach($eventtopic as $eventtopics)
            <a class="well btn wellall" href="{{ action("EventTopicsController@show", $eventtopics->id) }}">
                <h2>{{$eventtopics->name}}</h2>
                <ul class="list-group">
                    <li class="list-group-item">Naam: {{$eventtopics->name}}</li>
                </ul>
            </a>
            @endforeach
            <hr>
            {{$eventtopic->links()}}
        @else
            <h1>Geen onderwerpen gevonden</h1> 
            <strong><p>Voeg <a href="{{ action("EventTopicsController@create") }}">hier</a> een onderwerp toe!</p></strong>
    @endif
@endsection