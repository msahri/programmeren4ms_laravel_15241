@extends('layouts.crud')

@section('title')
        <title>Persoon</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/person">Persoon</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("PeopleController@index") }}">Alle Personen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Persoon toevoegen</h1>
{!! Form::open(['action' => 'PeopleController@store', 'method' => 'POST']) !!}
    <div class="row">
        <div class="col-mg-6 col-lg-6">
           <div class="form-group">
            {{Form::label('firstname', 'Voornaam')}}
            {{Form::text('firstname', '', ['class' => 'form-control ', 'placeholder' => 'Voer voornaam in!' ,'maxlength' => 80 ])}}
           </div>
           <div class="form-group">
            {{Form::label('birthday', 'Geboortedatum')}}
            {{Form::date('birthday', '', ['class' => 'form-control ' ])}}
           </div>
           <div class="form-group">
            {{Form::label('address1', 'Adres 1')}}
            {{Form::text('address1', '', ['class' => 'form-control ', 'placeholder' => 'Voer adres in!' ,'maxlength' => 90  ])}}
           </div>
           <div class="form-group">
            {{Form::label('address2', 'Adres 2')}}
            {{Form::text('address2', '', ['class' => 'form-control ', 'placeholder' => '(optioneel)Voer tweede adres in!' ,'maxlength' => 90  ])}}
           </div>
           <div class="form-group">
            {{Form::label('rating', 'Tevreden')}}
            {{Form::number('rating', '', ['class' => 'form-control formingnumber', 'placeholder' => 'Nr' , 'min' => 1, 'max' => 10 ])}}
            <small>Geef een number van 1 (heel ontevreden) tot 10 (heel tevreden)! </small>
           </div>
       </div>
       
       <div class="col-mg-6 col-lg-6">
           <div class="form-group">
            {{Form::label('lastname', 'Achternaam')}}
            {{Form::text('lastname', '', ['class' => 'form-control ', 'placeholder' => 'Voer achternaam in!' ,'maxlength' => 50  ])}}
           </div>
           <div class="form-group">
            {{Form::label('email', 'E-mail')}}
            {{Form::text('email', '', ['class' => 'form-control ', 'placeholder' => 'Voer email in!' ,'maxlength' => 50  ])}}
           </div>
           <div class="form-group">
            {{Form::label('postalcode', 'Postcode')}}
            {{Form::text('postalcode', '', ['class' => 'form-control ', 'placeholder' => 'Voer postcode in!' ,'maxlength' => 50  ])}}
           </div>
           <div class="form-group">
            {{Form::label('city', 'Stad')}}
            {{Form::text('city', '', ['class' => 'form-control ', 'placeholder' => 'Voer stad in!' ,'maxlength' => 50  ])}}
           </div>
           <div class="form-group">
            {{Form::label('phone1', 'Telefoon 1')}}
            {{Form::text('phone1', '', ['class' => 'form-control ', 'placeholder' => 'Voer telefoonnummer in!' ,'maxlength' => 50  ])}}
           </div>
       </div>
   </div>       
   <div>
       {{Form::submit('Persoon opslaan', ['class' => 'btn btn-primary btn-lg'])}}
   </div>
{!! Form::close() !!}
@endsection





