@extends('layouts.app')

@section('title')
        <title>Fric-Frac</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/">Fric-Frac</a>
@endsection

@section('navbarlinks')
        <li><a href="/event">Event</a></li>
        <li><a href="/eventTopic">Event Onderwerp</a></li> 
        <li><a href="/eventCategory">Event Categorieën</a></li>
        <li><a href="/person">Persoon</a></li>
        <li><a href="/role">Rol</a></li>
        <li><a href="/user">Gebruiker</a></li>
        <li><a href="/country">Landen</a></li>       
@endsection

@section('content')
        <div class="container centered">
            <div class="content">
                <div class="title">Fric-Frac</div>
            </div>
            <br>
            <div class="links" style="display:inline-block; text-decoration:none;">
                <a href="/event">Event |</a>
                <a href="/eventTopic">Event Onderwerp |</a>
                <a href="/eventCategory">Event Categorieën</a>
            </div>
            <br>
            <div class="links" style="display:inline-block; text-decoration:none;">
                <a href="/person">Persoon |</a>
                <a href="/role">Rol |</a>
                <a href="/user">Gebruiker |</a>
                <a href="/country">Landen</a>
            </div>
            
            <br>
                <footer id="footer" class="text-center">
                    <label>Made By Mehdi Sahri</label>
                </footer>
        </div>
@endsection