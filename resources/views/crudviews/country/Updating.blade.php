@extends('layouts.crud')

@section('title')
        <title>Country</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/country">Landen</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("CountriesController@index") }}">Alle Landen</a></li>
        <li><a class="navbarcrudlink" href="{{ action("CountriesController@create") }}">Land Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Wijzigen en Verwijderen</h1>
<h2>{{$country->name}}</h2>
    {!! Form::open(['action' => ['CountriesController@update', $country->id], 'method' => 'POST']) !!}
       <div class="form-group">
        {{Form::label('name', 'Naam')}}
        {{Form::text('name', $country->name, ['class' => 'form-control formingtext', 'maxlength' => 20])}}
       </div>
       <div class="form-group">
        {{Form::label('code', 'Code')}}
        {{Form::number('code', $country->code, ['class' => 'form-control formingnumber', 'min' => 1, 'max' => 200])}}
       </div>
       <div>
           {{Form::hidden('_method','PUT')}}
           {{Form::submit('Land Wijzigen', ['class' => 'btn btn-primary'])}}
       </div>
   {!! Form::close() !!}
   {!!Form::open(['action' => ['CountriesController@destroy', $country->id], 'method' => 'POST'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Land Verwijderen', ['class' => 'btn btn-danger'])}}
    {!!Form::close()!!}
@endsection



