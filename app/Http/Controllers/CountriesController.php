<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use DB;

use App\Http\Requests;

class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country = Country::paginate(5);
        
        return view('crudviews.country.ReadingAll')->with('country', $country);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('crudviews.country.Creating');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required'
            ]);
            
            // Create Country
        $country = new Country;
        $country->name = $request->input('name');
        $country->code = $request->input('code');
        
        //Save Country
        $country->save();
        
        // Redirect
        return redirect()->action('CountriesController@create')->with('success', 'Land Opgeslagen!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = Country::find($id);
        
        return view('crudviews.country.ReadingOne')->with('country', $country);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
        
        return view('crudviews.country.Updating')->with('country', $country);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required'
            ]);
            
            // Find Country
        $country = Country::find($id);
        $country->name = $request->input('name');
        $country->code = $request->input('code');
        
        //Save Country
        $country->save();
        
        // Redirect
        return redirect('/countries')->with('success', 'Land Gewijzigd!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find Country
        $country = Country::find($id);
        
        //Delete Country
        $country->delete();
        
        // Redirect
        return redirect('/countries')->with('deleted', 'Land Verwijderd!');
    }
}
