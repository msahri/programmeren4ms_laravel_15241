@extends('layouts.crud')

@section('title')
        <title>Rol</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/role">Rol</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("RolesController@index") }}">Alle Rollen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Rol toevoegen</h1>
{!! Form::open(['action' => 'RolesController@store', 'method' => 'POST']) !!}
   <div class="form-group">
    {{Form::label('name', 'Naam')}}
    {{Form::text('name', '', ['class' => 'form-control formingtext', 'placeholder' => 'Voer naam van rol in!' ,'maxlength' => 20 ])}}
   </div>
   <div>
       {{Form::submit('Rol opslaan', ['class' => 'btn btn-primary'])}}
   </div>
{!! Form::close() !!}
@endsection




