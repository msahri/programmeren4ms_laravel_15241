@extends('layouts.crud')

@section('title')
        <title>Persoon</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/person">Persoon</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("PeopleController@create") }}">Persoon Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    @if(count($person) > 0)
            @foreach($person as $people)
            <a class="well btn wellall" href="{{ action("PeopleController@show", $people->id) }}">
                        <h2>{{$people->lastname}} {{$people->firstname}}</h2>
                        <ul class="list-group">
                            <li class="list-group-item listitem">Voornaam: {{$people->firstname}}</li>
                            <li class="list-group-item listitem">Achternaam: {{$people->lastname}}</li>
                            <li class="list-group-item listitem">E-mail: {{$people->email}}</li>
                            <li class="list-group-item listitem">Adres 1: {{$people->address1}}</li>
                            <li class="list-group-item listitem">Adres 2: {{$people->address2}}</li>
                            <li class="list-group-item listitem">Postcode: {{$people->postalcode}}</li>
                            <li class="list-group-item listitem">Stad: {{$people->city}}</li>
                            <li class="list-group-item listitem">Telefoon 1: {{$people->phone1}}</li>
                            <li class="list-group-item listitem">Geboortedatum: {{$people->birthday}}</li>
                            <li class="list-group-item listitem">Tevreden: {{$people->rating}}</li>
                        </ul>   
            </a>
            @endforeach
            <hr>
            {{$person->links()}}
        @else
            <h1>Geen persoon gevonden</h1> 
            <strong><p>Voeg <a href="{{ action("PeopleController@create") }}">hier</a> een persoon toe!</p></strong>
    @endif
@endsection

