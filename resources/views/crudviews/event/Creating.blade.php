@extends('layouts.crud')

@section('title')
        <title>Event</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/event">Event</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventsController@index") }}">Alle Events</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Event toevoegen</h1>
{!! Form::open(['action' => 'EventsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="row">
        <div class="col-mg-6 col-lg-6">
           <div class="form-group">
            {{Form::label('name', 'Naam')}}
            {{Form::text('name', '', ['class' => 'form-control formingtext', 'placeholder' => 'Voer naam van event in!' ,'maxlength' => 80 ])}}
           </div>
           <div class="form-group">
            {{Form::label('location', 'Locatie')}}
            {{Form::text('location', '', ['class' => 'form-control formingtext', 'placeholder' => 'Voer locatie van event in!' ,'maxlength' => 90  ])}}
           </div>
           <div class="form-group">
            {{Form::label('starts', 'Start')}}
            {{Form::date('starts', '', ['class' => 'form-control formingtext' ])}}
           </div>
       </div>
       
       <div class="col-mg-6 col-lg-6">
           <div class="form-group">
            {{Form::label('organisername', 'Organisator naam')}}
            {{Form::text('organisername', '', ['class' => 'form-control formingtext', 'placeholder' => 'Voer organisator naam in!' ,'maxlength' => 50  ])}}
           </div>
           <div class="form-group">
            {{Form::label('organiserdescription', 'Organisator beschrijving')}}
            {{Form::text('organiserdescription', '', ['class' => 'form-control formingtext', 'placeholder' => 'Voer organisator beschrijving in!' ,'maxlength' => 90  ])}}
           </div>
           <div class="form-group">
            {{Form::label('ends', 'Einde')}}
            {{Form::date('ends', '', ['class' => 'form-control formingtext' ])}}
           </div>
       </div>
   </div>
           <div class="form-group">
            {{Form::label('description', 'Beschrijving')}}
            {{Form::textarea('description', '', ['class' => 'form-control formingmediumtext', 'placeholder' => 'Voer beschrijving van event in!' ])}}
           </div>
    <div class="form-group">
        {{Form::file('event_image')}}
    </div>       
   <div>
       {{Form::submit('Event opslaan', ['class' => 'btn btn-primary'])}}
   </div>
{!! Form::close() !!}
@endsection





