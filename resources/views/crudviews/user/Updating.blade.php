@extends('layouts.crud')

@section('title')
        <title>Gebruiker</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/user">Gebruiker</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("UsersController@index") }}">Alle Gebruikers</a></li>
        <li><a class="navbarcrudlink" href="{{ action("UsersController@create") }}">Gebruiker Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Wijzigen en Verwijderen</h1>
<h2>{{$user->name}}</h2>
    {!! Form::open(['action' => ['UsersController@update', $user->id], 'method' => 'POST']) !!}
       <div class="form-group">
        {{Form::label('name', 'Naam')}}
        {{Form::text('name', $user->name, ['class' => 'form-control formingtext', 'maxlength' => 20])}}
       </div>
       <div class="form-group">
        {{Form::label('email', 'E-mail')}}
        {{Form::text('email', $user->code, ['class' => 'form-control formingnumber', 'maxlength' => 20])}}
       </div>
       <div>
           {{Form::hidden('_method','PUT')}}
           {{Form::submit('Gebruiker Wijzigen', ['class' => 'btn btn-primary btn-lg'])}}
       </div>
   {!! Form::close() !!}
   {!!Form::open(['action' => ['UsersController@destroy', $user->id], 'method' => 'POST'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Gebruiker Verwijderen', ['class' => 'btn btn-danger'])}}
    {!!Form::close()!!}
@endsection
