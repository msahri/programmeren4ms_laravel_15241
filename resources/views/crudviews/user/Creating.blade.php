@extends('layouts.crud')

@section('title')
        <title>Gebruiker</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/user">Gebruiker</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("UsersController@index") }}">Alle Gebruikers</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    <div class="well">
        <div class="row">
            <div class="col-md-7 col-lg-7">
                <img src="/content/oops.png">
            </div>
            <div class="col-md-5 col-lg-5">
                <h3>Gebruikers kan je enkel met het registreersyteem aanmaken!</h3><br>
                <h3>Om een rol aan een gebruiker toe te wijzen, maak je eerst een gebruiker aan.</h3> 
                <h3>Dan ga je naar alle gebruikers en kies je desbetreffende gebruiker. 
                Tenslotte kan je op de wijzig en verwijder pagina de rol toewijzen.</h3>
            </div>   
        </div>        
    </div>
{!! Form::close() !!}
@endsection




