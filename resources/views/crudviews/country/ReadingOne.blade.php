@extends('layouts.crud')

@section('title')
        <title>Country</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/country">Landen</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("CountriesController@index") }}">Alle Landen</a></li>
        <li><a class="navbarcrudlink" href="{{ action("CountriesController@create") }}">Land Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    <h1>{{$country->name}}</h1>
        <a href="{{ action("CountriesController@edit", $country->id) }}">
            <ul class="list-group">
                <li class="list-group-item">Naam: {{$country->name}}</li>
                <li class="list-group-item">Code: {{$country->code}}</li>
                <li class="list-group-item">Aangemaakt op: {{$country->created_at}}</li>
            </ul>
        </a>
@endsection


