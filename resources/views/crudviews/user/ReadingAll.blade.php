@extends('layouts.crud')

@section('title')
        <title>Gebruiker</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/user">Gebruiker</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("UsersController@create") }}">Gebruiker Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    @if(count($user) > 0)
            @foreach($user as $users)
            <a class="well btn wellall" href="{{ action("UsersController@show", $users->id) }}">
                <h2>{{$users->name}}</h2>
                <ul class="list-group">
                    <li class="list-group-item">Naam: {{$users->name}}</li>
                    <li class="list-group-item">E-mail: {{$users->email}}</li>
                </ul>
            </a>
            @endforeach
            <hr>
            {{$user->links()}}
        @else
            <h1>Geen gebruikers gevonden</h1> 
            <strong><p>Voeg <a href="{{ action("UsersController@create") }}">hier</a> een gebruiker toe!</p></strong>
    @endif
@endsection