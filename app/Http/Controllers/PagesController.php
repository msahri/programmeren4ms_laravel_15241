<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    public function getFricFrac(){
        return view('fricfrac');
    }
    
    public function getEvent(){
        return view('event');
    }
    
    public function getEventTopic(){
        return view('eventTopic');
    }
    
    public function getEventCategory(){
        return view('eventCategory');
    }
    
    public function getPerson(){
        return view('person');
    }
    
    public function getUser(){
        return view('user');
    }
    
    public function getRole(){
        return view('role');
    }
    
    public function getCountry(){
        return view('country');
    }
    
    
}
