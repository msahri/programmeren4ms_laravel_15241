@extends('layouts.crud')

@section('title')
        <title>Country</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/country">Landen</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("CountriesController@index") }}">Alle Landen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Land toevoegen</h1>
{!! Form::open(['action' => 'CountriesController@store', 'method' => 'POST']) !!}
   <div class="form-group">
    {{Form::label('name', 'Naam')}}
    {{Form::text('name', '', ['class' => 'form-control formingtext', 'placeholder' => 'Voer naam van land in!' ,'maxlength' => 20 ])}}
   </div>
   <div class="form-group">
    {{Form::label('code', 'Code')}}
    {{Form::number('code', '', ['class' => 'form-control formingnumber', 'placeholder' => 'Nr' ,'min' => 1,'max' => 200  ])}}
   </div>
   <div>
       {{Form::submit('Land opslaan', ['class' => 'btn btn-primary btn-lg'])}}
   </div>
{!! Form::close() !!}
@endsection





