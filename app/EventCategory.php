<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    public function event(){
        return $this->hasMany('App\Event');
    }
}
