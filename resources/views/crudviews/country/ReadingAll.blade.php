@extends('layouts.crud')

@section('title')
        <title>Country</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/country">Landen</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("CountriesController@create") }}">Land Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    @if(count($country) > 0)
            @foreach($country as $countries)
            <a class="well btn wellall" href="{{ action("CountriesController@show", $countries->id) }}">
                <h2>{{$countries->name}}</h2>
                <ul class="list-group">
                    <li class="list-group-item">Naam: {{$countries->name}}</li>
                    <li class="list-group-item">Code: {{$countries->code}}</li>
                </ul>
            </a>
            @endforeach
            <hr>
            {{$country->links()}}
        @else
            <h1>Geen landen gevonden</h1> 
            <strong><p>Voeg <a href="{{ action("CountriesController@create") }}">hier</a> een land toe!</p></strong>
    @endif
@endsection

