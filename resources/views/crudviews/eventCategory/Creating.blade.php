@extends('layouts.crud')

@section('title')
        <title>Event Categorie</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/eventCategory">Event Categorie</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventCategoriesController@index") }}">Alle Categorieën</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Categorie toevoegen</h1>
{!! Form::open(['action' => 'EventCategoriesController@store', 'method' => 'POST']) !!}
   <div class="form-group">
    {{Form::label('name', 'Naam')}}
    {{Form::text('name', '', ['class' => 'form-control formingtext', 'placeholder' => 'Voer naam van categorie in!' ,'maxlength' => 20 ])}}
   </div>
   <div>
       {{Form::submit('Categorie opslaan', ['class' => 'btn btn-primary'])}}
   </div>
{!! Form::close() !!}
@endsection




