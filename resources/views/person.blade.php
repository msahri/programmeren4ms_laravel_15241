@extends('layouts.app')

@section('title')
        <title>Persoon</title>
        <link rel="icon" type="image/x-icon" href="favicon.ico">
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/">Fric-Frac</a>
@endsection

@section('navbarlinks')
        <li><a href="{{ action("PeopleController@create") }}">Persoon Toevoegen</a></li>
        <li><a href="{{ action("PeopleController@index") }}">Alle Personen</a></li>        
@endsection

@section('content')
        <div class="container centered">
            <div class="content">
                <div class="title">Persoon</div>
            </div>
            <br>
            <div class="links" style="display:inline-block; text-decoration:none;">
                <a href="{{ action("PeopleController@create") }}">Persoon Toevoegen |</a>
                <a href="{{ action("PeopleController@index") }}">Alle Personen</a>
            </div>
            <br>
            <div class="links" style="display:inline-block; text-decoration:none;">
                <a href="/">Fric-Frac</a>
            <!--    <a href="/role">ActionNamebvCreate</a>-->
            <!--    <a href="/user">ActionNamebvCreate</a>-->
            </div>
            
            <br>
                <footer id="footer" class="text-center">
                    <label>Made By Mehdi Sahri</label>
                </footer>
        </div>
@endsection