@extends('layouts.crud')

@section('title')
        <title>Rol</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/role">Rol</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("RolesController@create") }}">Rol Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    @if(count($role) > 0)
            @foreach($role as $roles)
            <a class="well btn wellall" href="{{ action("RolesController@show", $roles->id) }}">
                <h2>{{$roles->name}}</h2>
                <ul class="list-group">
                    <li class="list-group-item">Naam: {{$roles->name}}</li>
                </ul>
            </a>
            @endforeach
            <hr>
            {{ $role->links() }}
        @else
            <h1>Geen rollen gevonden</h1> 
            <strong><p>Voeg <a href="{{ action("RolesController@create") }}">hier</a> een rol toe!</p></strong>
    @endif
@endsection