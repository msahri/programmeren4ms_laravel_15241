@extends('layouts.crud')

@section('title')
        <title>Persoon</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/person">Persoon</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("PeopleController@index") }}">Alle Personen</a></li>
        <li><a class="navbarcrudlink" href="{{ action("PeopleController@create") }}">Persoon Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Wijzigen en Verwijderen</h1>
<h2>{{$person->lastname}} {{$person->firstname}}</h2> 
    {!! Form::open(['action' => ['PeopleController@update', $person->id], 'method' => 'POST']) !!}
       <div class="row">
        <div class="col-mg-6 col-lg-6">
           <div class="form-group">
            {{Form::label('firstname', 'Voornaam')}}
            {{Form::text('firstname', $person->firstname, ['class' => 'form-control ', 'placeholder' => 'Voer voornaam in!' ,'maxlength' => 80 ])}}
           </div>
           <div class="form-group">
            {{Form::label('birthday', 'Geboortedatum')}}
            {{Form::date('birthday', $person->birthday, ['class' => 'form-control ' ])}}
           </div>
           <div class="form-group">
            {{Form::label('address1', 'Adres 1')}}
            {{Form::text('address1', $person->address1, ['class' => 'form-control ', 'placeholder' => 'Voer adres in!' ,'maxlength' => 90  ])}}
           </div>
           <div class="form-group">
            {{Form::label('address2', 'Adres 2')}}
            {{Form::text('address2', $person->address2, ['class' => 'form-control ', 'placeholder' => '(optioneel)Voer tweede adres in!' ,'maxlength' => 90  ])}}
           </div>
           <div class="form-group">
            {{Form::label('rating', 'Tevreden')}}
            {{Form::number('rating', $person->rating, ['class' => 'form-control formingnumber', 'placeholder' => 'Nr' , 'min' => 1, 'max' => 10 ])}}
            <small>Geef een number van 1 (heel ontevreden) tot 10 (heel tevreden)! </small>
           </div>
       </div>
       
       <div class="col-mg-6 col-lg-6">
           <div class="form-group">
            {{Form::label('lastname', 'Achternaam')}}
            {{Form::text('lastname', $person->lastname, ['class' => 'form-control ', 'placeholder' => 'Voer achternaam in!' ,'maxlength' => 50  ])}}
           </div>
           <div class="form-group">
            {{Form::label('email', 'E-mail')}}
            {{Form::text('email', $person->email, ['class' => 'form-control ', 'placeholder' => 'Voer email in!' ,'maxlength' => 50  ])}}
           </div>
           <div class="form-group">
            {{Form::label('postalcode', 'Postcode')}}
            {{Form::text('postalcode', $person->postalcode, ['class' => 'form-control ', 'placeholder' => 'Voer postcode in!' ,'maxlength' => 50  ])}}
           </div>
           <div class="form-group">
            {{Form::label('city', 'Stad')}}
            {{Form::text('city', $person->city, ['class' => 'form-control ', 'placeholder' => 'Voer stad in!' ,'maxlength' => 50  ])}}
           </div>
           <div class="form-group">
            {{Form::label('phone1', 'Telefoon 1')}}
            {{Form::text('phone1', $person->phone1, ['class' => 'form-control ', 'placeholder' => 'Voer telefoonnummer in!' ,'maxlength' => 50  ])}}
           </div>
       </div>
   </div>
       <div>
           {{Form::hidden('_method','PUT')}}
           {{Form::submit('Persoon Wijzigen', ['class' => 'btn btn-primary btn-lg'])}}
       </div>
   {!! Form::close() !!}
   {!!Form::open(['action' => ['PeopleController@destroy', $person->id], 'method' => 'POST'])!!}
   <br><br><br>
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Persoon Verwijderen', ['class' => 'btn btn-danger'])}}
    {!!Form::close()!!}
@endsection

@section('sidebar')
    
@endsection

