<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function eventcategory(){
        return $this->belongsTo('App\EventCategory');
    }
    
    public function eventtopic(){
        return $this->belongsTo('App\EventTopic');
    }
}
