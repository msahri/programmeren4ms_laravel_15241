@extends('layouts.app')

@section('title')
        <title>Country</title>
        <link rel="icon" type="image/x-icon" href="favicon.ico">
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/country">Landen</a>
@endsection

@section('navbarlinks')
        <li><a href="{{ action("CountriesController@create") }}">Land Toevoegen</a></li>
        <li><a href="{{ action("CountriesController@index") }}">Alle Landen</a></li>        
@endsection

@section('content')
        <div class="container centered">
            <div class="content">
                <div class="title">Landen</div>
            </div>
            <br>
            <div class="links" style="display:inline-block; text-decoration:none;">
                <a href="{{ action("CountriesController@create") }}">Land Toevoegen |</a>
                <a href="{{ action("CountriesController@index") }}">Alle Landen</a>
            </div>
            <br>
            <div class="links" style="display:inline-block; text-decoration:none;">
                <a href="/">Fric-Frac</a>
            <!--    <a href="/role">ActionNamebvCreate</a>-->
            <!--    <a href="/user">ActionNamebvCreate</a>-->
            </div>
            
            <br>
                <footer id="footer" class="text-center">
                    <label>Made By Mehdi Sahri</label>
                </footer>
        </div>
@endsection