@extends('layouts.crud')

@section('title')
        <title>Event Categorie</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/eventCategory">Event Categorie</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventCategoriesController@index") }}">Alle Categorieën</a></li>
        <li><a class="navbarcrudlink" href="{{ action("EventCategoriesController@create") }}">Categorie Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Wijzigen en Verwijderen</h1>
<h2>{{$eventcategory->name}}</h2>
    {!! Form::open(['action' => ['EventCategoriesController@update', $eventcategory->id], 'method' => 'POST']) !!}
       <div class="form-group">
        {{Form::label('name', 'Naam')}}
        {{Form::text('name', $eventcategory->name, ['class' => 'form-control formingtext', 'maxlength' => 20])}}
       </div>
       <div>
           {{Form::hidden('_method','PUT')}}
           {{Form::submit('Categorie Wijzigen', ['class' => 'btn btn-primary'])}}
       </div>
   {!! Form::close() !!}
   {!!Form::open(['action' => ['EventCategoriesController@destroy', $eventcategory->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Categorie Verwijderen', ['class' => 'btn btn-danger btn-sm'])}}
    {!!Form::close()!!}
@endsection



