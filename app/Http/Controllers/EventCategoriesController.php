<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventCategory;
use DB;

use App\Http\Requests;

class EventCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventcategory = EventCategory::paginate(5);
        
        return view('crudviews.eventCategory.ReadingAll')->with('eventcategory', $eventcategory);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('crudviews.eventCategory.Creating');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);
            
            // Create EventCategory
        $eventcategory = new EventCategory;
        $eventcategory->name = $request->input('name');
        
        //Save EventCategory
        $eventcategory->save();
        
        // Redirect
        return redirect()->action('EventCategoriesController@create')->with('success', 'Event categorie Opgeslagen!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $eventcategory = EventCategory::find($id);
        
        return view('crudviews.eventCategory.ReadingOne')->with('eventcategory', $eventcategory);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventcategory = EventCategory::find($id);
        
        return view('crudviews.eventCategory.Updating')->with('eventcategory', $eventcategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);
            
            // Find EventCategory
        $eventcategory = EventCategory::find($id);
        $eventcategory->name = $request->input('name');
        
        //Save EventCategory
        $eventcategory->save();
        
        // Redirect
        return redirect('/eventcategories')->with('success', 'Event categorie Gewijzigd!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find EventCategory
        $eventcategory = EventCategory::find($id);
        
        //Delete EventCategory
        $eventcategory->delete();
        
        // Redirect
        return redirect('/eventcategories')->with('deleted', 'Event categorie Verwijderd!');
    }
}
