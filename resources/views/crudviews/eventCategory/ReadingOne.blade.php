@extends('layouts.crud')

@section('title')
        <title>Event Categorie</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/eventCategory">Event Categorie</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventCategoriesController@index") }}">Alle Categorieën</a></li>
        <li><a class="navbarcrudlink" href="{{ action("EventCategoriesController@create") }}">Land Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    <h1>{{$eventcategory->name}}</h1>
        <a href="{{ action("EventCategoriesController@edit", $eventcategory->id) }}">
            <ul class="list-group">
                <li class="list-group-item">Naam: {{$eventcategory->name}}</li>
                <li class="list-group-item">Aangemaakt op: {{$eventcategory->created_at}}</li>
            </ul>
        </a>
@endsection

