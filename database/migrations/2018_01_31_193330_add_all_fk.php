<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('people', function($table){
            $table->integer('country_id');
        });
        
        Schema::table('users', function($table){
            $table->integer('person_id');
        });
        
        Schema::table('users', function($table){
            $table->integer('role_id');
        });
        
        Schema::table('events', function($table){
            $table->integer('event_category_id');
        });
        
        Schema::table('events', function($table){
            $table->integer('event_topic_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('people', function($table){
            $table->dropColumn('country_id');
        });
        
        Schema::table('users', function($table){
            $table->dropColumn('person_id');
        });
        
        Schema::table('users', function($table){
            $table->dropColumn('role_id');
        });
        
        Schema::table('events', function($table){
            $table->dropColumn('event_category_id');
        });
        
        Schema::table('events', function($table){
            $table->dropColumn('event_topic_id');
        });
    }
}
