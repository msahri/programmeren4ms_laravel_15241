<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => ['web']], function () {

        // -------------Main-----------
        
        Route::get('/', 'PagesController@getFricFrac');
        
        Route::get('/event', 'PagesController@getEvent');
        
        Route::get('/eventTopic', 'PagesController@getEventTopic');
        
        Route::get('/eventCategory', 'PagesController@getEventCategory');
        
        Route::get('/person', 'PagesController@getPerson');
        
        Route::get('/user', 'PagesController@getUser');
        
        Route::get('/role', 'PagesController@getRole');
        
        Route::get('/country', 'PagesController@getCountry');
        
        
        // -------------CRUD-----------
        
        // --------COUNTRY---------
        
        Route::resource('countries', 'CountriesController');
        
        
        // --------Event---------
        
        Route::resource('events', 'EventsController');
        
        
        // --------EventCategory---------
        
        Route::resource('eventcategories', 'EventCategoriesController');
        
        
        // --------EventTopic---------
        
        Route::resource('eventtopics', 'EventTopicsController');
        
        // --------Role---------
        
        Route::resource('roles', 'RolesController');
        
        // --------Person---------
        
        Route::resource('people', 'PeopleController');
        
        // --------User---------
        
        Route::resource('users', 'UsersController');
        
        // -------------Auth-----------
        
        Route::auth();
        
        Route::get('/home', 'HomeController@index');

});