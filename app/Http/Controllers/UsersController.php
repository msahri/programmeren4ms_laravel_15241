<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

use App\Http\Requests;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::paginate(5);
        
        return view('crudviews.user.ReadingAll')->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('crudviews.user.Creating');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'salt' => 'required'
            ]);
            
            // Create User
        $user = new User;
        $user->name = $request->input('name');
        $user->code = $request->input('email');
        $user->code = $request->input('password');
        $user->code = $request->input('salt');
        
        //Save User
        $user->save();
        
        // Redirect
        return redirect()->action('UsersController@create')->with('success', 'Gebruiker Opgeslagen!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        
        return view('crudviews.user.ReadingOne')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        
        return view('crudviews.user.Updating')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'salt' => 'required'
            ]);
            
            // Find User
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->code = $request->input('email');
        $user->code = $request->input('password');
        $user->code = $request->input('salt');
        
        //Save User
        $user->save();
        
        // Redirect
        return redirect('/users')->with('success', 'Gebruiker Gewijzigd!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find User
        $user = User::find($id);
        
        //Delete User
        $user->delete();
        
        // Redirect
        return redirect('/users')->with('deleted', 'Gebruiker Verwijderd!');
    }
}
