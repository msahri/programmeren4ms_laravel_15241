@extends('layouts.crud')

@section('title')
        <title>Rol</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/role">Rol</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("RolesController@index") }}">Alle Rollen</a></li>
        <li><a class="navbarcrudlink" href="{{ action("RolesController@create") }}">Rol Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    <h1>{{$role->name}}</h1>
        <a href="{{ action("RolesController@edit", $role->id) }}">
            <ul class="list-group">
                <li class="list-group-item">Naam: {{$role->name}}</li>
                <li class="list-group-item">Aangemaakt op: {{$role->created_at}}</li>
            </ul>
        </a>
@endsection

