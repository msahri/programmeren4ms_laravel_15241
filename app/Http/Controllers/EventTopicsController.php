<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventTopic;
use DB;

use App\Http\Requests;

class EventTopicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventtopic = EventTopic::paginate(5);
        
        return view('crudviews.eventTopic.ReadingAll')->with('eventtopic', $eventtopic);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('crudviews.eventTopic.Creating');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);
            
            // Create EventTopic
        $eventtopic = new EventTopic;
        $eventtopic->name = $request->input('name');
        
        //Save EventTopic
        $eventtopic->save();
        
        // Redirect
        return redirect()->action('EventTopicsController@create')->with('success', 'Event onderwerp Opgeslagen!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $eventtopic = EventTopic::find($id);
        
        return view('crudviews.eventTopic.ReadingOne')->with('eventtopic', $eventtopic);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventtopic = EventTopic::find($id);
        
        return view('crudviews.eventTopic.Updating')->with('eventtopic', $eventtopic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);
            
            // Find EventTopic
        $eventtopic = EventTopic::find($id);
        $eventtopic->name = $request->input('name');
        
        //Save EventTopic
        $eventtopic->save();
        
        // Redirect
        return redirect('/eventtopics')->with('success', 'Event onderwerp Gewijzigd!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find EventTopic
        $eventtopic = EventTopic::find($id);
        
        //Delete EventTopic
        $eventtopic->delete();
        
        // Redirect
        return redirect('/eventtopics')->with('deleted', 'Event onderwerp Verwijderd!');
    }
}
