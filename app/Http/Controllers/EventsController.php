<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Http\Requests;

use App\Event;

use DB;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = Event::paginate(5);
        
        return view('crudviews.event.ReadingAll')->with('event', $event);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = Event::with('eventcategory')->get();
        
        foreach ($event as $events) {
            echo $events->eventcategory->name;
        }
           
        return view('crudviews.event.Creating')->with('event', $event);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
            'starts' => 'required',
            'ends' => 'required',
            'event_image' => 'image|max:1999',
            'description' => 'required',
            'organisername' => 'required',
            'organiserdescription' => 'required'
            ]);
            
            // Handle Image Upload
            if ($request->hasFile('event_image')) {
                //Get filename with extension
                $filenameWithExt = $request->file('event_image')->getClientOriginalName();
                //Get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                //Get just extension
                $extension=$request->file('event_image')->getClientOriginalExtension();
                //Filename to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;
                //Upload Image
                $path= $request->file('event_image')->move('storage/event_images', $fileNameToStore);
                
            }else {
                $fileNameToStore = 'noimage.jpg';
            }
            
            
            // Create Event
        $event = new Event;
        $event = Event::with('eventcategory')->get();
        
        $event->name = $request->input('name');
        $event->location = $request->input('location');
        $event->starts = $request->input('starts');
        $event->ends = $request->input('ends');
        $event->event_image = $fileNameToStore;
        $event->description = $request->input('description');
        $event->organisername = $request->input('organisername');
        $event->organiserdescription = $request->input('organiserdescription');
        
        foreach ($event as $events) {
            echo $events->eventcategory->name;
        }
        
        //Save Event
        $event->save();
        
        // Redirect
        return redirect()->action('EventsController@create')->with('success', 'Event Opgeslagen!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        
        return view('crudviews.event.ReadingOne')->with('event', $event);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        
        return view('crudviews.event.Updating')->with('event', $event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
            'starts' => 'required',
            'ends' => 'required',
            'event_image' => 'image|max:1999',
            'description' => 'required',
            'organisername' => 'required',
            'organiserdescription' => 'required'
            ]);
            
            // Handle Image Upload
            if ($request->hasFile('event_image')) {
                //Get filename with extension
                $filenameWithExt = $request->file('event_image')->getClientOriginalName();
                //Get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                //Get just extension
                $extension=$request->file('event_image')->getClientOriginalExtension();
                //Filename to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;
                //Upload Image
                $path= $request->file('event_image')->move('storage/event_images', $fileNameToStore);
            }
            
            // Find Event
        $event = Event::find($id);
        $event = Event::with('eventcategory')->get();
        
        $event->name = $request->input('name');
        $event->location = $request->input('location');
        $event->starts = $request->input('starts');
        $event->ends = $request->input('ends');
        $event->description = $request->input('description');
        $event->organisername = $request->input('organisername');
        $event->organiserdescription = $request->input('organiserdescription');
        if ($request->hasFile('event_image')){
            $event->event_image = $fileNameToStore;
        }
        
        foreach ($event as $events) {
            echo $events->eventcategory->name;
        }
        
        //Save Event
        $event->save();
        
        // Redirect
        return redirect('/events')->with('success', 'Event Gewijzigd!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find Event
        $event = Event::find($id);
        
        //Delete Image As WELL
        if ($event->event_image != 'noimage.jpg') {
            Storage::delete('storage/event_images/'.$event->event_image);
        }
        //Delete Event
        $event->delete();
        
        // Redirect
        return redirect('/events')->with('deleted', 'Event Verwijderd!');
    }
}
