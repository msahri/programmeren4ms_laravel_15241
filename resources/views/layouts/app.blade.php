<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @yield('title')

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                font-weight: 100;
                font-family: 'Raleway', sans-serif;
                
            }
            .container {
                text-align: center; 
                display: table-cell;
                vertical-align: middle;
            }
a    {
        text-decoration:none;
     }
a:hover {
            text-decoration: none;
        }

            .content {
                text-align: center; 
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
            
            .links{
                font-size: 30px;
            }
            .label{
                font-size: 20px;
            }
            .fa-btn {
            margin-right: 6px;
            }
            .centered{
                position: fixed;
                top: 30%;
                left: 20%;
                right: 25%;
                bottom: 25%;
            }
            #footer{
                position:fixed;
                top: 90%;
                left: 5%;
                width:100%;
                height:60px;
                text-align: left;
            }
        </style>
</head>
<body id="app-layout">
    
        @include('inc.navbar')

        @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
