<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     @yield('title')
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/app.css" type="text/css" />
    <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<style>
body {
        margin: 0;
        padding: 0;
        width: 100%;
        font-weight: 100;
        font-family: 'Raleway', sans-serif;
     }
a    {
        text-decoration:none;
     }
a:hover {
            text-decoration: none;
        }
     .navbarcrud{
         background: DimGray;
     }
     .navbarcrudlink{
         color: White; 
     }
     .navbarcrudbrandlink{
        color: WhiteSmoke;
         
    }
     .wellall{
         color: DimGray;
     }
     .formingnumber{
         width:75px;
     }
     .formingtext{
         width:200px;
     }
     .formingmediumtext{
         width:500px;
     }
     .listitem{
         list-style-position:inside;
         white-space: nowrap;
         overflow: hidden;
         text-overflow: ellipsis;   
     }
     #footer{
                position:absolute;
                bottom:0;
                width:100%;
                height:60px;
                text-align: center;
            }
</style>
<body>
    @include('inc.navbar')
    
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-lg-9">
                @include('inc.messages')
                @yield('content')
            </div>
            <div class="col-md-1 col-lg-1">
            </div>
            <div class="col-md-2 col-lg-2">
                @yield('sidebar')
            </div>
        </div>
    </div>
</body>
</html>