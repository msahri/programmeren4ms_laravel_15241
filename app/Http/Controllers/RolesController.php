<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use DB;

use App\Http\Requests;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::paginate(5);
        
        return view('crudviews.role.ReadingAll')->with('role', $role);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('crudviews.role.Creating');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);
            
            // Create Role
        $role = new Role;
        $role->name = $request->input('name');
        
        //Save Role
        $role->save();
        
        // Redirect
        return redirect()->action('RolesController@create')->with('success', 'Rol Opgeslagen!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        
        return view('crudviews.role.ReadingOne')->with('role', $role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        
        return view('crudviews.role.Updating')->with('role', $role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);
            
            // Find Role
        $role = Role::find($id);
        $role->name = $request->input('name');
        
        //Save Role
        $role->save();
        
        // Redirect
        return redirect('/roles')->with('success', 'Rol Gewijzigd!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find Role
        $role = Role::find($id);
        
        //Delete Role
        $role->delete();
        
        // Redirect
        return redirect('/roles')->with('deleted', 'Rol Verwijderd!');
    }
}
