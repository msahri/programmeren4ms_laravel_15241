@extends('layouts.crud')

@section('title')
        <title>Onderwerp</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/eventTopic">Event Onderwerp</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventTopicsController@index") }}">Alle Onderwerpen</a></li>
        <li><a class="navbarcrudlink" href="{{ action("EventTopicsController@create") }}">Onderwerp Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Wijzigen en Verwijderen</h1>
<h2>{{$eventtopic->name}}</h2>
    {!! Form::open(['action' => ['EventTopicsController@update', $eventtopic->id], 'method' => 'POST']) !!}
       <div class="form-group">
        {{Form::label('name', 'Naam')}}
        {{Form::text('name', $eventtopic->name, ['class' => 'form-control formingtext', 'maxlength' => 20])}}
       </div>
       <div>
           {{Form::hidden('_method','PUT')}}
           {{Form::submit('Onderwerp Wijzigen', ['class' => 'btn btn-primary'])}}
       </div>
   {!! Form::close() !!}
   {!!Form::open(['action' => ['EventTopicsController@destroy', $eventtopic->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Onderwerp Verwijderen', ['class' => 'btn btn-danger btn-sm'])}}
    {!!Form::close()!!}
@endsection



