<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Person;

use DB;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $person = Person::paginate(5);
        
        return view('crudviews.person.ReadingAll')->with('person', $person);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('crudviews.person.Creating');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'address1' => 'required',
            'postalcode' => 'required',
            'city' => 'required',
            'phone1' => 'required',
            'birthday' => 'required',
            'rating' => 'required'
            ]);
            
            
            // Create Person
        $person = new Person;
        $person->firstname = $request->input('firstname');
        $person->lastname = $request->input('lastname');
        $person->email = $request->input('email');
        $person->address1 = $request->input('address1');
        $person->address2 = $request->input('address2');
        $person->postalcode = $request->input('postalcode');
        $person->city = $request->input('city');
        $person->phone1 = $request->input('phone1');
        $person->birthday = $request->input('birthday');
        $person->rating = $request->input('rating');
        
        //Save Person
        $person->save();
        
        // Redirect
        return redirect()->action('PeopleController@create')->with('success', 'Persoon Opgeslagen!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = Person::find($id);
        
        return view('crudviews.person.ReadingOne')->with('person', $person);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $person = Person::find($id);
        
        return view('crudviews.person.Updating')->with('person', $person);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'address1' => 'required',
            'postalcode' => 'required',
            'city' => 'required',
            'phone1' => 'required',
            'birthday' => 'required',
            'rating' => 'required'
            ]);
            
            // Find Person
        $person = Person::find($id);
        $person->firstname = $request->input('firstname');
        $person->lastname = $request->input('lastname');
        $person->email = $request->input('email');
        $person->address1 = $request->input('address1');
        $person->address2 = $request->input('address2');
        $person->postalcode = $request->input('postalcode');
        $person->city = $request->input('city');
        $person->phone1 = $request->input('phone1');
        $person->birthday = $request->input('birthday');
        $person->rating = $request->input('rating');
        
        //Save Person
        $person->save();
        
        // Redirect
        return redirect('/people')->with('success', 'Persoon Gewijzigd!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find Person
        $person = Person::find($id);
        
        
        //Delete Person
        $person->delete();
        
        // Redirect
        return redirect('/people')->with('deleted', 'Persoon Verwijderd!');
    }
}
