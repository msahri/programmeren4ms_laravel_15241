@extends('layouts.crud')

@section('title')
        <title>Event Categorie</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/eventCategory">Event Categorie</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventCategoriesController@create") }}">Categorie Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    @if(count($eventcategory) > 0)
            @foreach($eventcategory as $eventcategories)
            <a class="well btn wellall" href="{{ action("EventCategoriesController@show", $eventcategories->id) }}">
                <h2>{{$eventcategories->name}}</h2>
                <ul class="list-group">
                    <li class="list-group-item">Naam: {{$eventcategories->name}}</li>
                </ul>
            </a>
            @endforeach
            <hr>
            {{$eventcategory->links()}}
        @else
            <h1>Geen categorieën gevonden</h1> 
            <strong><p>Voeg <a href="{{ action("EventCategoriesController@create") }}">hier</a> een categorie toe!</p></strong>
    @endif
@endsection

