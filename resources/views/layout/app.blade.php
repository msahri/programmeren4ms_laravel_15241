
<!DOCTYPE html>
<html>
    <head>
        @yield('title')
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="/css/app.css" type="text/css" />
        <link rel="icon" type="image/x-icon" href="favicon.ico">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Raleway', sans-serif;
            }
            .container {
                text-align: center; 
                display: table-cell;
                vertical-align: middle;
            }
a    {
        text-decoration:none;
     }
a:hover {
            text-decoration: none;
        }

            .content {
                text-align: center; 
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
            
            .links{
                font-size: 30px;
            }
            .label{
                font-size: 20px;
            }
            #footer{
                position:absolute;
                bottom:0;
                width:100%;
                height:60px;
                text-align: center;
            }
        </style>
    </head>
    <body>
        @yield('content')
    </body>
</html>
