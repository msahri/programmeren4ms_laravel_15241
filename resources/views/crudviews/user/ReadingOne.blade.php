@extends('layouts.crud')

@section('title')
        <title>Gebruiker</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/user">Gebruiker</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("UsersController@index") }}">Alle Gebruikers</a></li>
        <li><a class="navbarcrudlink" href="{{ action("UsersController@create") }}">Gebruiker Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    <h1>{{$user->name}}</h1>
        <a href="{{ action("UsersController@edit", $user->id) }}">
            <ul class="list-group">
                <li class="list-group-item">Naam: {{$user->name}}</li>
                <li class="list-group-item">E-mail: {{$user->email}}</li>
                <li class="list-group-item">Aangemaakt op: {{$user->created_at}}</li>
            </ul>
        </a>
@endsection