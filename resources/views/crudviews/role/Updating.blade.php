@extends('layouts.crud')

@section('title')
        <title>Rol</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/role">Rol</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("RolesController@index") }}">Alle Rollen</a></li>
        <li><a class="navbarcrudlink" href="{{ action("RolesController@create") }}">Rol Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Wijzigen en Verwijderen</h1>
<h2>{{$role->name}}</h2>
    {!! Form::open(['action' => ['RolesController@update', $role->id], 'method' => 'POST']) !!}
       <div class="form-group">
        {{Form::label('name', 'Naam')}}
        {{Form::text('name', $role->name, ['class' => 'form-control formingtext', 'maxlength' => 20])}}
       </div>
       <div>
           {{Form::hidden('_method','PUT')}}
           {{Form::submit('Rol Wijzigen', ['class' => 'btn btn-primary'])}}
       </div>
   {!! Form::close() !!}
   {!!Form::open(['action' => ['RolesController@destroy', $role->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Rol Verwijderen', ['class' => 'btn btn-danger btn-sm'])}}
    {!!Form::close()!!}
@endsection



