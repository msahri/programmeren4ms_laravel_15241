@extends('layouts.crud')

@section('title')
        <title>Persoon</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/person">Persoon</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("PeopleController@index") }}">Alle Personen</a></li>
        <li><a class="navbarcrudlink" href="{{ action("PeopleController@create") }}">Persoon Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    <h1>{{$person->lastname}} {{$person->firstname}}</h1>
        <a href="{{ action("PeopleController@edit", $person->id) }}">
            <div class="row">  
                    <div class="col-md-8 col-sm-8">
                        <ul class="list-group">
                            <li class="list-group-item">Voornaam: {{$person->firstname}}</li>
                            <li class="list-group-item">Achternaam: {{$person->lastname}}</li>
                            <li class="list-group-item">E-mail: {{$person->email}}</li>
                            <li class="list-group-item">Adres 1: {{$person->address1}}</li>
                            <li class="list-group-item">Adres 2: {{$person->address2}}</li>
                            <li class="list-group-item">Postcode: {{$person->postalcode}}</li>
                            <li class="list-group-item">Stad: {{$person->city}}</li>
                            <li class="list-group-item">Telefoon 1: {{$person->phone1}}</li>
                            <li class="list-group-item">Geboortedatum: {{$person->birthday}}</li>
                            <li class="list-group-item">Tevreden: {{$person->rating}}</li>
                        </ul>
                    </div>     
                </div>
        </a>
@endsection


