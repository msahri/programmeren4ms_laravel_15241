@extends('layouts.crud')

@section('title')
        <title>Onderwerp</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/eventTopic">Event Onderwerp</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventTopicsController@index") }}">Alle Onderwerpen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
<h1>Onderwerp toevoegen</h1>
{!! Form::open(['action' => 'EventTopicsController@store', 'method' => 'POST']) !!}
   <div class="form-group">
    {{Form::label('name', 'Naam')}}
    {{Form::text('name', '', ['class' => 'form-control formingtext', 'placeholder' => 'Voer naam van onderwerp in!' ,'maxlength' => 20 ])}}
   </div>
   <div>
       {{Form::submit('Onderwerp opslaan', ['class' => 'btn btn-primary'])}}
   </div>
{!! Form::close() !!}
@endsection




