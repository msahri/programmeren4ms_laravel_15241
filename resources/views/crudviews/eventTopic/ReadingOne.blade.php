@extends('layouts.crud')

@section('title')
        <title>Onderwerp</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/eventTopic">Event Onderwerp</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventTopicsController@index") }}">Alle Onderwerpen</a></li>
        <li><a class="navbarcrudlink" href="{{ action("EventTopicsController@create") }}">Onderwerp Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    <h1>{{$eventtopic->name}}</h1>
        <a href="{{ action("EventTopicsController@edit", $eventtopic->id) }}">
            <ul class="list-group">
                <li class="list-group-item">Naam: {{$eventtopic->name}}</li>
                <li class="list-group-item">Aangemaakt op: {{$eventtopic->created_at}}</li>
            </ul>
        </a>
@endsection

