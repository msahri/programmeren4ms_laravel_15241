@extends('layouts.crud')

@section('title')
        <title>Event</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/event">Events</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventsController@index") }}">Alle Events</a></li>
        <li><a class="navbarcrudlink" href="{{ action("EventsController@create") }}">Event Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    <h1>{{$event->name}}</h1>
        <a href="{{ action("EventsController@edit", $event->id) }}">
            <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <img style="width:100%" src="/storage/event_images/{{$event->event_image}}">
                    </div>    
                    <div class="col-md-8 col-sm-8">
                        <ul class="list-group">
                            <li class="list-group-item">Naam: {{$event->name}}</li>
                            <li class="list-group-item">Locatie: {{$event->location}}</li>
                            <li class="list-group-item">Aangemaakt op: {{$event->created_at}}</li>
                        </ul>
                    </div>     
                </div>
        </a>
@endsection


