@extends('layouts.crud')

@section('title')
        <title>Event</title>
@endsection

@section('projname')
        <a class="navbar-brand navbarcrudbrandlink" href="/event">Event</a>
@endsection

@section('navbarlinks')
        <li><a class="navbarcrudlink" href="{{ action("EventsController@create") }}">Event Toevoegen</a></li>
        <li><a class="navbarcrudlink" href="/">Fric-Frac</a></li>        
@endsection

@section('content')
    @if(count($event) > 0)
            @foreach($event as $events)
            <a class="well btn wellall" href="{{ action("EventsController@show", $events->id) }}">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <img style="width:100%" src="/storage/event_images/{{$events->event_image}}">
                    </div>    
                    <div class="col-md-8 col-sm-8">
                        <h2>{{$events->name}}</h2>
                        <ul class="list-group">
                            <li class="list-group-item">Naam: {{$events->name}}</li>
                            <li class="list-group-item">Locatie: {{$events->location}}</li>
                        </ul>
                    </div>     
                </div>        
            </a>
            @endforeach
            <hr>
            {{$event->links()}}
        @else
            <h1>Geen event gevonden</h1> 
            <strong><p>Voeg <a href="{{ action("EventsController@create") }}">hier</a> een event toe!</p></strong>
    @endif
@endsection

